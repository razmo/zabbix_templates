#!/bin/bash

# передается 3 параметра
#директория
#группа бэкапов
#что вернуть (имя последнего архива, размер архива или дата модификации) name|size|time

dir="$1"
backupname="$2"
res="$3"

if [[ -z "$res" && ! ( "$res" = "name" || "$res" = "size" || "$res" = "time" ) ]]; then 
	res="name"
fi

if [ -n "$backupname" -a  -d "$dir" ]; then	
	newest=$(ls -lt ${dir}/${backupname}* | awk 'NR == 1 {print $9}')
	
	if [ "$res" = "name" ]; then
		echo "$(basename ${newest})"
	elif [ "$res" = "size" ]; then
		bsize=$(ls -lt ${newest} | awk '{print $5}')
		if [ -z "$bsize" ]; then
			bsize=0
		fi
		echo "$bsize"
	elif [ "$res" = "time" ]; then
		mtime=$(stat ${newest} | awk '/Modify:/ {print $2,$3}' | cut -f1 -d"." )
		sec=$(date -d "$mtime" "+%s")
		echo "$sec"
	fi
else
	echo "Null"
fi
