#!/bin/bash

echo '{ "data":[ '

dir="$1"
if [ -d "${dir}" ]; then	
	readarray groups < <(find "${dir}" -iname "*.tar.gz" -exec basename '{}' \; | cut -d'-' -f1 | sort -u )
	length=${#groups[@]}
	let maxiter=length-1

	for (( i=0; i<$length; i++ )); do 			
		if [ ${i} -eq ${maxiter} ]; then
			delim=""
		else
			delim="," 
		fi
		item=$(echo "${groups[i]}" | tr -d '\n')
		echo "{ \"{#BACKUPNAME}\":\"${item}\" }${delim}"
	done
fi

echo ']}'
